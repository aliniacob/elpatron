import uiEvents from './uiEvents';
import patronGraph from './patronGraph';
import PostRow from './buildUi';
import utils from './utils';

const ui = {

	/**
	* updateAvatar updates the users avatar photo after an new upload
	*/
	'updateAvatar': (trigger) => {

		if (!trigger) {
			return;
		}

		const avatar = document.getElementById('avatars');

		uiEvents.updateAvatar(avatar);

	},
	/**
	* uploadPhoto sends the photo to the server's storage
	*/
	'uploadPhoto': () => {

		const form = document.getElementById('uploadForm');
		const inp = document.getElementById('uploadImage');
		const selectButton = document.getElementById('avatars');

		uiEvents.photoSubmit(form, inp, selectButton);

	},

	// 'getPatronList': () => {
	//
	// 	uiEvents.getPatrons();
	//
	// },

	/**
	* getPosts get's the current logged in user photoSubmit
	* @param {Bolean} rebuild - if true it will refresh the posts
	*/
	'getPosts': (rebuild) => {
		let payload = {
			'ownPosts': true
		};

		let cb = (postIds) => {
			let aggPosts = utils.postAggregator(postIds);
			new PostRow(aggPosts);
		}
		uiEvents.getPostsIds(payload, cb);
	},

	/**
	* buildPatronGraph builds the wall users list sorted by appreciations
	* @param {Object} patrons - all registered users
	*/
	'buildPatronGraph': (patrons, trigger) => {

		if (!trigger) {
			return;
		}

		const graphContainer = document.getElementById('patron-graph');

		patronGraph(graphContainer, patrons.userList);

	},

	/**
	* wallpostToggler toggles the new wallpost form
	*/
	'wallpostToggler': () => {

		const newPostButton = document.getElementById('new-post');
		const closePostButton = document.getElementById('closeWallPost');
		const formContainer = document.getElementById('wall-post');

		uiEvents.toggleVisibilityOn(newPostButton, formContainer);
		uiEvents.toggleVisibilityOff(closePostButton, formContainer);

	},
	/**
	* wallpostSubmit submits the new posts to users profile
	*/
	'wallpostSubmit': () => {

		const formContainer = document.getElementById('wall-post');
		const formular = document.getElementById('wallForm');
		const inp = document.getElementById('uploadmedia');
		const selectButton = document.getElementById('new-profile-post');
		const postButton = document.getElementById('post-to-profile');

		uiEvents.postToProfile(formular, inp, selectButton, postButton, formContainer);

	}

};

export default ui;
