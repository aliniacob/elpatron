import request from './req';

const netRequest = {
	'fetch': async (params) => {
		  try {
		    return await request(params);
		  } catch (e) {
		    console.warn(e);
		  }
		}
};

export default netRequest;
