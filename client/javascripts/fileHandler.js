let fileHandler = {

	/**
	* getMime gets the file mimetype
	* @param {Object} f - the file
	*/
	'getMime': (f) => {
		return f.mimetype;
	},

	/**
	* setUriPreview sets an thumbnail type preview from an image uploaded via form element
	* @param {Object} evt - the input change envet
	* @param {Object} previewer - target element to insert the thumb preview
	*/
	'setUriPreview': (evt, previewer) => {

		let files = evt.target.files; // FileList object

		for (let i = 0, f; f = files[i]; i++) {


			if (!f.type.match('image.*')) {
				continue;
			}

			const reader = new FileReader();

			reader.onload = ((theFile) => {
				return (e) => {
					previewer.innerHTML = '<div class="thumb" style="background-image:url(' + e.target.result + ')"></div>';
				};
			})(f);

			reader.readAsDataURL(f);
		}
	}
}

export default fileHandler;
