let request = (param) => {
	const method = param.method;
	const url = param.url;
	const formdata = param.formdata;
	const payload = JSON.stringify(param.payload) || {};

  return new Promise((resolve, reject) => {
    let req = new XMLHttpRequest();

    req.open(method, url);
    req.withCredentials = true;

    req.onload = () => {
      if (req.status == 200) {
        resolve(req.response);
      } else {
        reject(Error(req.statusText));
      }
    };

    req.onerror = () => {
      reject(Error('Network Error'));
    };

		if (formdata) {
			req.send(formdata);
		} else {
			req.setRequestHeader('Content-Type', 'application/json');
			req.send(payload);
		}
  });
};



export default request;
