import moment from 'moment';
import range from 'moment-range';

let utils = {
	'unbindEvents': (options) => {
		for (let i = 0; i < options.length; i += 1) {
			let el = options[i].el;
			let fu = options[i].fu;
			let ev = options[i].ev;

			if (el.removeEventListener) {
				el.removeEventListener(`${ev}`, fu);
			} else if (x.detachEvent) {
				el.detachEvent(`on + ${ev}`, fu);
			}
		}
	},

	/**
	* getReadableDate transforms milliseconds date format to readable date
	* @param {String} mils - the milliseconds format
	* @return {String} concatDate - returns readable date DD/MM/YYYY
	*/
	'getReadableDate': (mils) => {
		let milsIn = parseInt(mils);
		let time = new Date(milsIn);

		let theyear = time.getFullYear();
		let themonth = time.getMonth() + 1;
		let thetoday = time.getDate();
		let concatDate = thetoday + '/' + themonth + '/' + theyear;

		return concatDate;
	},

	/**
	* splitInChunks - splits an array into chunkSize
	* @param {Array} arr - flat array
	* @param {Number} chunkSize - desired chunk size
	* @return {Array} return an Array containing chunks of Arrays
	*/
	'splitInChunks': (arr, chunkSize) => {
    let groups = [];
		let i;
    for (i = 0; i < arr.length; i += chunkSize) {
        groups.push(arr.slice(i, i + chunkSize));
    }
    return groups;
	},

	/**
	* sortPosts gets all available posts and sorts them by date
	* @param {Array} postsArr - combined arrays of posts (user and followers)
	* @return {String} - returns a new array with posts sorted by posting time
	*/
	'sortPosts': (postsArr) => {
		let flatten = [].concat.apply([], postsArr);
		let sorted = flatten.sort((a, b) => {
			return a.split('-')[0] > b.split('-')[0];
		});
		return sorted;
	},

	/**
	* groupPosts groups posts by days with maximum 20/day
	* @param {Array} postsArr - sorted(by date) arrays of posts (user and followers)
	* @return {Object} - returns a new object with posts grouped
	*/
	'groupPosts': (posts) => {
		let transformed = posts.map((post) => {
			// Split the post so we can create a new date from it
			let splitted = post.split('-')
			return [
				moment(splitted[0], 'x').format('DD-MM-YYYY'),
				splitted[1],
				splitted[0]
			];
		}).reduce((a, b) => {
			// Reduce the array to an Object containing posts stored in Date keys
			if (a.hasOwnProperty(`${b[0]}`)) {
				a[`${b[0]}`].push(`${b[2]}-${b[1]}`);
			} else {
				a[`${b[0]}`] = [`${b[2]}-${b[1]}`];
			}
			return a;
		}, {});

		// Transform the previous object and split it's key content (posts)
		// in groups of 20 or whatever size we provide
		Object.keys(transformed).forEach((key) => {
			transformed[key] = utils.splitInChunks(transformed[key], 10);
		});

		return transformed;
	},

	/**
	* Function generator to iterate trough posts groupped by day
	* @param {Array} daysArray - daysObject key values
	* @param {Object} daysObject - days with post grouped by days
	*/
	'daysIterator': function *(posts) {
		let current = 0;

		while(current < posts.daysArray.length) {
			yield {
			'currentDay': `${posts.daysArray[current]}`,
			'posts':	posts.daysObject[`${posts.daysArray[current]}`]
			};
			current += 1;
		}
	},

	/**
	* Function generator to iterate trough postsArray
	* @param {Array} postsArray - contains posts previously splitted in chunks
	*/
	'postIterator': function *(postsArray) {
			let current = 0;
			while (current < postsArray.length) {
				yield postsArray[current];
				current += 1;
			}
	},

	/**
	* Sorts and aggregates posts so we can then use generators on them
	* @param {Object} posts - posts abjects and available days
	* @return {object} sorted posts and aggregated by dates
	*/
	'postAggregator': (posts) => {

		let grouppedPosts = utils.groupPosts(utils.sortPosts(posts));
		let availableDates = [];

		Object.keys(grouppedPosts).forEach((key) => {
			availableDates.push(key);
		});
		let aggPosts = {
			'daysArray': availableDates,
			'daysObject': grouppedPosts
		};

		return aggPosts;
	}

};

export default utils;
