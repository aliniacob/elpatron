import request from './req';
import ui from './ui';


document.addEventListener("DOMContentLoaded", function() {
	for (let key in ui) {
		ui[key]();
	}
});
