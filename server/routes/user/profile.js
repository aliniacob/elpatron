
import express from 'express';
import api from './../../api/index';

const router = express.Router();

router.get('/', (req, res) => {
	res.render('profile', {
		user: req.user
	});
});

router.use('/api', (req, res, next) => {
	if (!req.isAuthenticated()) {
		return res.redirect('/login');
	}
	next();
}, api);

export default router;
