import express from 'express';
import register from './user/register';
import login from './user/login';
import logout from './user/logout';
import profile from './user/profile';

const router = express.Router();

router.use('/login', login);
router.use('/logout', logout);
router.use('/register', register);

router.get('/', (req, res) => {
	res.render('index', { });
});

router.use('/profile', (req, res, next) => {
	if (!req.isAuthenticated()) {
		return res.redirect('/login');
	}
	next();
}, profile);

export default router;
