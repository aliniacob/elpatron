import express from 'express';
import Account from '../../models/account';

const router = express.Router();

router.get('/', (req, res) => {
	Account.find({}, (err, users) => {
		res.json({
			'name': req.user.username,
			'email': req.user.email,
			'userList': users
		});
	});
});

export default router;
