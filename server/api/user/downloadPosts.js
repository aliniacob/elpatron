import express from 'express';
import buildPostList from '../requestProcessor/buildPostList';
import path from 'path';
import fs from 'fs';

const router = express.Router();

/**
*	Route to get current user's posts
*/

router.post('/', (req, res) => {
	let userIds = [req.user.id];
	let payload = req.body;

	if (! payload.ownPosts) {
		req.user.follows.forEach((followed) => {
			userIds.push(followed);
		});
	}
	let callBackFunc = (col) => {
		res.send(col);
	}
	let postObject = buildPostList(req.body.postList, callBackFunc);
});

router.get('/rawPostsIds', (req, res) => {
	res.send(req.user.posts);
});

router.get('/:fileId', (req, res) => {
	const file = `${path.resolve('./')}/usrUploads/posts/${req.user.id}/${req.params.fileId}`;

	fs.stat(file, (err, stats) => {
		if (stats === undefined) {
			res.send('No file attached to this post');
		} else {
			res.sendFile(file);
		}
	});
});


export default router;
