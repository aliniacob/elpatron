import express from 'express';
import multer from 'multer';
import path from 'path';
import fs from 'fs';
import mkdirp from 'mkdirp';
import lwip from 'lwip';

const router = express.Router();

const fileFilter = (req, file, callback) => {
	let mimes = 'image/gif, image/jpeg, image/png, image/bmp';
	if (mimes.match(`${file.mimetype}`)) {
		callback(null, true);
	}
};

const storage = multer.diskStorage({
	'destination': (req, file, callback) => {
		const userPath = path.resolve('./') + '/usrUploads/avatars/' + `${req.user.id}`;
		mkdirp(userPath, () => {
			callback(null, userPath);
		});
	},
	'filename': (req, file, callback) => {
		callback(null, 'avatar');
	}
});


const genThumbnail = (location, mime, filepath, width, height, cb) => {

	fs.readFile(filepath, function(err, buffer){
		lwip.open(buffer, mime, (err, image) => {
			let widthRatio = width / image.width();
			let heightRatio = height / image.height();
			let ratio = Math.max(widthRatio, heightRatio);
			image
			.batch()
			.scale(ratio)
			.crop(width, height)
			.writeFile(`${filepath}-thumb.jpg`, 'jpg', function(err){
				if (!err) {
					cb('success');
				}
			});
		});
	});
};

const upload = multer({storage: storage, fileFilter: fileFilter}).single('avatar');

router.post('/', (req, res) => {
	upload(req, res, (err) => {
		const location = path.resolve('./') + '/usrUploads/avatars/' + `${req.user.id}/`;
		const filepath = `${location}avatar`;
		const mime = `${req.file.mimetype.replace('image/', '')}`;

		if(err) {
			return res.end("Error uploading avatar");
		}

		let responseStatus = (stat) => {
			if (stat) {
				res.send('success');
			}
		}

		genThumbnail(location, mime, filepath, 100, 100, responseStatus);
	});
});

router.get('/', (req, res) => {
	console.log(req.query.type);
	let file = `${path.resolve('./')}/usrUploads/avatars/${req.user.id}/avatar`;

	if (req.query.type) {
		file += `-${req.query.type}.jpg`;
	}

	let defaultAvatar = `${path.resolve('./')}/usrUploads/avatars/default/avatar`;

	fs.stat(file, (err, stats) => {
		if (stats === undefined) {
			res.sendFile(defaultAvatar);
		} else {
			res.sendFile(file);
		}
	});

});

export default router;
