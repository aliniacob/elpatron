import express from 'express';
import gUserMeta from './user/userMeta';
import uploadAvatar from './user/uploadAvatar';
import postToProfile from './user/postToProfile';
import downloadPosts from './user/downloadPosts';
import submitLike from './user/submitLike';

const router = express.Router();

router.use('/getUserMeta', gUserMeta);
router.use('/uploadAvatar', uploadAvatar);
router.use('/postToProfile', postToProfile);
router.use('/downloadPosts', downloadPosts);
router.use('/submitLike', submitLike);

export default router;
