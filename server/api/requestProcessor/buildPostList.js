import path from 'path';
import fs from 'fs';
import Account from '../../models/account';
import Post from '../../models/posts';


/**
*	Function to retrieve user's posts
*	@param {rawIds} id's stored in Account Schema - posts
*	@param {userId} current logged in user
*	@param {cb} callback with founded results
*/

const buildPostList = (rawIds, cb) => {
	let idsToFind = [];

	for (let i = 0; i < rawIds.length; i += 1) {
		let postId = rawIds[i].split('-')[1];
		idsToFind.push(postId);
	}

	Post.find({_id: { $in:  idsToFind}}, (err, file) => {
		if (err) {
			return res.end('Cannot find the owner of this post')
		}

		let resObj = {};
		let len = file.length - 1;
		for (let i = len; i >= 0; i -= 1) {
			let post = file[i];
				let meta = {
					'ref': `${post.id}`,
					'author': `${post.ownerName}`,
					'description': `${post.description}`,
					'comments': `${post.comments}`,
					'likes': `${post.likes}`,
					'mediaPath': `${post.attachedMedia}`
				}
				resObj[file[i].attachedMedia] = meta;
		}
		cb(resObj);
	});
};

export default buildPostList;
