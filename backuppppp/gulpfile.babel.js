import gulp from 'gulp';
import shell from 'gulp-shell';
import rimraf from 'rimraf';
import run from 'run-sequence';
import watch from 'gulp-watch';
import server from 'gulp-live-server';
import less from 'gulp-less';
import source from 'vinyl-source-stream';
import browserify from 'browserify';

const paths = {
  js: ['./server/**/*.js'],
  jade: ['./server/**/*.jade'],
  client: ['./client/**/*.js'],
  img: ['./client/**/**/*.+(jpg|jpeg|gif|png|ico)'],
	less: ['./client/**/style.less'],
  lessPart: ['./client/**/**/*.less'],
  serverDest: './app/server',
  clientDest: './app/client',
  allFiles: './app'
}


gulp.task('default', cb => {
  run('server', 'build', 'watch', cb);
});

gulp.task('build', cb => {
  run('clean', 'babel', 'babelify', 'views', 'images', 'less', 'restart', cb);
});

gulp.task('clean', cb => {
  rimraf(paths.allFiles, cb);
});

gulp.task('less', () => {
  return gulp.src(paths.less)
    .pipe(less({ paths: './client/stylesheets/partials' }))
    .pipe(gulp.dest(paths.clientDest));
});

gulp.task('babel', shell.task([
  'babel server --out-dir app/server',
  'babel client/javascripts/homepage --out-dir app/client/javascripts'
]));

gulp.task('babelify', () => {
  return browserify({
    entries: './client/javascripts/app.js',
    extensions: ['.js'], debug: true})
    .transform('babelify', {
			presets: ['es2015', 'react'],
			plugins: [
				"syntax-async-functions",
				"transform-async-to-generator",
				"transform-regenerator",
				"transform-runtime"
			]
		})
    .bundle()
    .pipe(source('app.js'))
    .pipe(gulp.dest('./app/client/javascripts'));
});

gulp.task('images', () => {
  return gulp.src(paths.img)
    .pipe(gulp.dest(paths.clientDest));
});

gulp.task('views', () => {
  return gulp.src(paths.jade)
    .pipe(gulp.dest(paths.serverDest));
});

let express;

gulp.task('server', () => {
  express = server.new('./app/server/server.js');
});

gulp.task('restart', () => {
  express.start.bind(express)();
});

gulp.task('watch', () => {
  watch(paths.js, () => {gulp.start('build');});
	watch(paths.less, () => {gulp.start('build');});
  watch(paths.lessPart, () => {gulp.start('build');});
  watch(paths.jade, () => {gulp.start('build');});
  watch(paths.client, () => {gulp.start('build');});
});
