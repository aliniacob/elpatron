import netRequest from './netReq';
import handleMedia from './fileHandler';
import ui from './ui';
import buildUi from './buildUi';

const uiEvents = {

	/**
	* updateAvatar updates the users avatar upon new submission
	* @param {Object} avat - the avatar img DOM refference
	*/
	'updateAvatar': (avat) => {

		const options = {
			'method': 'GET',
			'url': 'http://localhost:3000/profile/api/uploadAvatar?type=thumb',
			'formdata': false
		};
		netRequest.fetch(options)
		.then((res) => {
			let src = avat.src;
			avat.src = src;
		})
	},

	/**
	* photoSubmit sends an photo (avatar mainly) to the server
	* The photo is automatically uploaded upon selection
	* @param {Object} form - the form element
	* @param {Object} inp - the file input element
		@param {Object} selectButton - fires the click event on the input element
	*/
	'photoSubmit': (form, inp, selectButton) => {
		selectButton.addEventListener('click', (e) => {
			inp.click();
		});

		inp.addEventListener('change', (e) => {
			let	fdata = new FormData(form);
			const options = {
				'method': 'POST',
				'url': 'http://localhost:3000/profile/api/uploadAvatar',
				'formdata': fdata
			};
			netRequest.fetch(options)
			.then((res) => {
				if (res === 'success') {
					ui.updateAvatar(true);
				}
			});
		});
	},

	/**
	* toggleVisibilityOn removes an CSS class over an item
	* @param {Object} button - the element that triggers the action
	* @param {Object} affectedItem - the element to be changed
	*/
	'toggleVisibilityOn': (button, affectedItem) => {
		button.addEventListener('click', () => {
			affectedItem.classList.remove('hide-offcanvas')
		});
	},

	/**
	* toggleVisibilityOff adds an CSS class over an item
	* @param {Object} button - the element that triggers the action
	* @param {Object} affectedItem - the element to be changed
	*/
	'toggleVisibilityOff': (button, affectedItem) => {
		button.addEventListener('click', () => {
			affectedItem.classList.add('hide-offcanvas')
		});
	},

	/**
	* postToProfile makes an new wallpost
	* @param {Object} postForm - the form element
	* @param {Object} inp - the 'type' file input element
	* @param {Object} selectButton - the element that will open the file selection window
	* @param {Object} postButton - post submission button
	* @param {Object} formContainer - the main form's DOM element wrapper
	*/
	'postToProfile': (postForm, inp, selectButton, postButton, formContainer) => {
		let selectButtonBckp = selectButton.innerHTML;
		selectButton.addEventListener('click', () => {
			inp.click();
		});

		inp.addEventListener('change', (e) => {
			handleMedia.setUriPreview(e, selectButton);
		});

		postButton.addEventListener('click', () => {
			let	fdata = new FormData(postForm);
			const options = {
				'method': 'POST',
				'url': 'http://localhost:3000/profile/api/postToProfile',
				'formdata': fdata
			};
			netRequest.fetch(options)
			.then((res) => {
				if (res === 'success') {
					fdata = null;
					postForm.reset();
					selectButton.innerHTML = selectButtonBckp;
					formContainer.classList.toggle('hide-offcanvas');
					ui.getOwnPosts(true);
				}
			});
		});
	},

	/**
	* getPatrons gets all registered users and sends the result to the DOM
	* builder function
	*/
	'getPatrons': () => {
		const options = {
			'method': 'GET',
			'url': 'http://localhost:3000/profile/api/getUserMeta',
			'formdata': false
		};
		netRequest.fetch(options)
		.then((res) => {
			ui.buildPatronGraph(JSON.parse(res), true);
		})
	},

	/**
	* getMyPosts get's current logged in users wall posts
	* @param {Object} cb - callbavk containing the user's posts result
	*/
	'getMyPosts': (cb) => {
		const options = {
			'method': 'GET',
			'url': 'http://localhost:3000/profile/api/getOwnPosts',
			'formdata': false
		};
		netRequest.fetch(options)
		.then((res) => {
			cb(JSON.parse(res));
		})
	},

	/**
	* getMyPosts get's current logged in users wall posts
	* @param {Object} cb - callbavk containing the user's posts result
	*/

	'submitLike': (postReference) => {
		const options = {
			'method': 'POST',
			'url': `http://localhost:3000/profile/api/submitLike?ref=${postReference}`,
			'formdata': false
		};
		netRequest.fetch(options)
		.then((res) => {
			console.log('UPDATED');
		})
	}
};

export default uiEvents;
