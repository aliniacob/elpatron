import request from './req';
import ui from './ui';


document.addEventListener("DOMContentLoaded", function() {
	init();
});

const init = () => {
	for (let key in ui) {
		ui[key]();
	}
};
