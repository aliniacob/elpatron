import handleMedia from './fileHandler';
import utils from './utils';
import uiEvents from './uiEvents';

class DomPost {
	constructor(info) {
		console.log(info);
		this.post = info;
		this.postWrapper = document.createElement('div');
		this.header = document.createElement('header');
		this.footer = document.createElement('footer');

		this.postWrapper.setAttribute('id', `${this.post.ref}`);
		this.postWrapper.classList.add('post-wrapper');
	}


	doHeader() {
		let headerContent = document.createElement('div');
		headerContent.classList.add('header-content');

		let authorAvatar = document.createElement('img');
		authorAvatar.src = '/../profile/api/uploadAvatar?type=thumb';

		let headerAuthor = document.createElement('p');
		headerAuthor.classList.add('header-author');
		headerAuthor.innerHTML = `<span>${this.post.author}</span>`;

		let dateCreated = document.createElement('p');
		dateCreated.classList.add('header-date');
		dateCreated.innerHTML = 'on ' + utils.getReadableDate(this.post.mediaPath);

		headerContent.appendChild(authorAvatar);
		headerContent.appendChild(headerAuthor);
		headerContent.appendChild(dateCreated);

		return headerContent;
	}

	doMedia() {
		let content = document.createElement('div');
		content.classList.add('image-wrapper');

		let image = new Image();
		let apiSrc = window.location.href + '/api/getOwnPosts/';
		let imgLocation = apiSrc + `${this.post.mediaPath}`;

		image.onload = () => {
			content.appendChild(image);
		}
		image.src = imgLocation;

		return content;
	}

	doMeta() {
		let postsMeta = document.createElement('div');
		postsMeta.classList.add('post-meta');

		let postDesc = document.createElement('p');
		postDesc.classList.add('post-desc');
		postDesc.innerHTML = `${this.post.description}`;

		postsMeta.appendChild(postDesc);

		return postsMeta;

	}

	doSocialStat() {
		let socialStat = document.createElement('div');
		socialStat.classList.add('social-stat');

		let likes = document.createElement('p');
		likes.classList.add('likes');
		likes.innerHTML = `<span>${this.post.likes.length}</span>
											<i class="material-icons">whatshot</i>`;

		let comments = document.createElement('p');
		comments.classList.add('comments');
		comments.innerHTML = `<span>${this.post.comments.length}</span>
												<i class="material-icons">comment</i>`;

		socialStat.appendChild(likes);
		socialStat.appendChild(comments);

		return socialStat;
	}

	doPostActions() {
		let postActions = document.createElement('div');
		postActions.classList.add('post-actions');
		postActions.setAttribute('data-target-post', `${this.post.ref}`);

		let likeButton = document.createElement('p');
		likeButton.classList.add('submit-like');
		likeButton.innerHTML = `<i class="material-icons">whatshot</i>`;

		likeButton.addEventListener('click', () => {
			uiEvents.submitLike(`${this.post.ref}`);
		});

		let commentButton = document.createElement('p');
		commentButton.classList.add('submit-comment');
		commentButton.innerHTML = `<i class="material-icons">comment</i>`;

		commentButton.addEventListener('click', () => {
			// uiEvents.toggleCommentPopup(`${this.post.ref}`);
		});

		postActions.appendChild(likeButton);
		postActions.appendChild(commentButton);

		return postActions;
	}

	getDom() {
		this.postWrapper.appendChild(this.doHeader());
		this.postWrapper.appendChild(this.doMedia());
		this.postWrapper.appendChild(this.doMeta());
		this.postWrapper.appendChild(this.doSocialStat());
		this.postWrapper.appendChild(this.doPostActions());

		return this.postWrapper;
	}

}

export default DomPost;
