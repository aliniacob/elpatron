let utils = {
	'unbindEvents': (options) => {
		for (let i = 0; i < options.length; i += 1) {
			let el = options[i].el;
			let fu = options[i].fu;
			let ev = options[i].ev;

			if (el.removeEventListener) {
				el.removeEventListener(`${ev}`, fu);
			} else if (x.detachEvent) {
				el.detachEvent(`on + ${ev}`, fu);
			}
		}
	},
	/**
	getReadableDate transforms milliseconds date format to readable date
	* @param {String} mils - the milliseconds format
	* @return {String} concatDate - returns readable date DD/MM/YYYY
	*/
	'getReadableDate': (mils) => {
		let milsIn = parseInt(mils);
		let time = new Date(milsIn);

		let theyear = time.getFullYear();
		let themonth = time.getMonth() + 1;
		let thetoday = time.getDate();
		let concatDate = thetoday + '/' + themonth + '/' + theyear;

		return concatDate;
	}
};

export default utils;
