let request = (param) => {
	const method = param.method;
	const url = param.url;
	const formdata = param.formdata;

  return new Promise((resolve, reject) => {
    let req = new XMLHttpRequest();

    req.open(method, url);
    req.withCredentials = true;

    req.onload = () => {
      if (req.status == 200) {
        resolve(req.response);
      } else {
        reject(Error(req.statusText));
      }
    };

    req.onerror = () => {
      reject(Error("Network Error"));
    };

		let fd = new FormData();

		if (formdata) {
			req.send(formdata);
		} else {
			req.send();
		}
  });
};



export default request;
