import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import favicon from 'serve-favicon';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import mongoose from 'mongoose';
import passport from 'passport';
import passportLocal from 'passport-local';
import expressSession from 'express-session';

//Import default route
import routes from './routes/index';

// import default account model
import Account from './models/account';

const app = express();
const LocalStrategy = passportLocal.Strategy;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.locals.pretty = true;
// uncomment after placing your favicon in /public
app.use(express.static('./app/client'));
app.use(favicon(path.join('./app/client', 'images', 'favicon.ico')));
app.use(logger('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.use(expressSession({
    secret: '!@#ERDFC$fvst345656dvshgudscxvghf$%&*dcfv',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

app.use('/', routes);

// passport config
passport.use(new LocalStrategy(Account.authenticate()));
passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());

// mongoose
mongoose.connect('mongodb://127.0.0.1:2701');

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  res.redirect('/');
  next(err);
});

const server = app.listen(3000, function () {
  console.log('Express listening on port 3000');
});
