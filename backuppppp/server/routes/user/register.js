
import express from 'express';
import passport from 'passport';
import Account from '../../models/account';

const router = express.Router();

router.post('/', (req, res) => {
	Account.register(new Account({
		username: req.body.username,
		email: req.body.email
	}), req.body.password, (err, account) => {
		if (err) {
			return res.render('index', {info: 'Sorry. That user already exists. Try again.', retry: true});
		}
		passport.authenticate('local')(req, res, () => {
			res.redirect('/profile');
		});
	});
});

export default router;
