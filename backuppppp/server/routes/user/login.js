
import express from 'express';
import passport from 'passport';

const router = express.Router();

router.post('/', (req, res, next) => {
	passport.authenticate('local', (err, user, info) => {
		if (err) {
			return next(err);
		}
		if (!user) {
			return res.render('index', {reason: 'Invalid username or password'});
		}
		req.logIn(user, function(err) {
			if (err) {
				return next(err);
			}
			return res.redirect('/profile');
		});
	})(req, res, next);
});

export default router;
