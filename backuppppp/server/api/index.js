import express from 'express';
import gUserMeta from './user/userMeta';
import uploadAvatar from './user/uploadAvatar';
import postToProfile from './user/postToProfile';
import getOwnPosts from './user/getOwnPosts';

const router = express.Router();

router.use('/getUserMeta', gUserMeta);
router.use('/uploadAvatar', uploadAvatar);
router.use('/postToProfile', postToProfile);
router.use('/getOwnPosts', getOwnPosts);

export default router;
