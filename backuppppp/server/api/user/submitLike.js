import express from 'express';
import Account from '../../models/account';
import Post from '../../models/posts';

const router = express.Router();

/**
*	Route to get current user's posts
*/

router.get('/', (req, res) => {
	res.send(req.query.ref)
});



export default router;
