import express from 'express';
import processOwnPost from '../requestProcessor/processOwnUserPosts'
import path from 'path';
import fs from 'fs';

const router = express.Router();

/**
*	Route to get current user's posts
*/

router.get('/', (req, res) => {
	let postsList = req.user.posts;
	let userId = req.user.id;

	let callBackFunc = (col) => {
		res.send(col);
	}
	let postObject = processOwnPost(postsList, userId, callBackFunc);
});

router.get('/:fileId', (req, res) => {
	const file = path.resolve('./') +
						'/usrUploads/posts/' + `${req.user.id}/` + `${req.params.fileId}`;

	fs.stat(file, (err, stats) => {
		if (stats === undefined) {
			res.send('No file attached to this post');
		} else {
			res.sendFile(file);
		}
	});
});


export default router;
