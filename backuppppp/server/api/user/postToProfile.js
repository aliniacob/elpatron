import express from 'express';
import multer from 'multer';
import path from 'path';
import fs from 'fs';
import mkdirp from 'mkdirp';
import Account from '../../models/account';
import Post from '../../models/posts';

const router = express.Router();

let fileId = '';

/**
*	Filters the uploaded file to match one of the acceptable mimes
*	@param {file} the uploaded file
*/

const fileFilter = (req, file, callback) => {
	let mimes = 'image/gif, image/jpeg, image/png, image/bmp';
	if (mimes.match(`${file.mimetype}`)) {
		callback(null, true);
	}
};

/**
* Set the default storage location
* Creates an folder for each user named with it's db Account id
*/

const storage = multer.diskStorage({
	'destination': (req, file, callback) => {
		const userPath = path.resolve('./') + '/usrUploads/posts/' + `${req.user.id}/`;
		mkdirp(userPath, () => {
			callback(null, userPath);
		});
	},
	'filename': (req, file, callback) => {
		callback(null, `${fileId}`);
	}
});

/**
*	POST rounte for uploading and saving the newly created post
* Each file is stored with an timestamp
*/

router.post('/', (req,res) => {
	fileId = Date.now();

	const upload = multer({
		storage: storage,
		fileFilter: fileFilter
	}).single('media');

	upload(req, res, (err) => {
		if(err) {
			return res.end("Error proccesing uploaded file");
		}
		let newPost = new Post({
			owner: req.user.id,
			ownerName: req.user.username,
			description: req.body.comment,
			attachedMedia: fileId,
			likes: [],
			comments: []
		});

		/**
		* Save's new post in the database
		*	@param {product} the current saved item
		*/

		newPost.save((err, product) => {
			if (err) {
				return res.end('Could not save the new post as db document');
			}

			Account.findById(req.user.id, (err, acc) => {
				if (err) {
					return res.end('Cannot find the owner of this post')
				}
				acc.posts.push(`${fileId}` + `-${product._id}`);
				acc.save((err) => {
					if (err) {
						return res.end('Could not save users reference post id');
					}
					res.end('success');
				});
			});

		});
	});
});

export default router;
