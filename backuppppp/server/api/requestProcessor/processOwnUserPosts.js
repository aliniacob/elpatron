import path from 'path';
import fs from 'fs';
import Account from '../../models/account';
import Post from '../../models/posts';


/**
*	Function to retrieve current user's posts
*	@param {rawIds} id's stored in Account Schema - posts
*	@param {userId} current logged in user
*	@param {cb} callback with founded results
*/

const processOwnPosts = (rawIds, userId, cb) => {
	Post.find({owner: `${userId}`}, (err, file) => {
		if (err) {
			return res.end('Cannot find the owner of this post')
		}
		let resObj = {};
		let len = file.length - 1;
		for (let i = len; i >= 0; i -= 1) {
			let post = file[i];
				let meta = {
					'ref': `${post.id}`,
					'author': `${post.ownerName}`,
					'description': `${post.description}`,
					'comments': `${post.comments}`,
					'likes': `${post.likes}`,
					'mediaPath': `${post.attachedMedia}`
				}
				resObj[file[i].attachedMedia] = meta;
		}
		cb(resObj);
	});
};

export default processOwnPosts;
