
import mongoose from 'mongoose';
import passportLocalMongoose from 'passport-local-mongoose';

const Schema = mongoose.Schema;

const Account = new Schema({
    username: String,
    email: String,
    password: String,
    userData: String,
		followers: Array,
		follows: Array,
		created: {
			type: Date,
			default: Date.now()
		},
		posts: Array
});

Account.plugin(passportLocalMongoose, {
  'usernameQueryFields': ['email']
});

export default mongoose.model('Account', Account);
