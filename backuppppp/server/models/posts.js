import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const Post = new Schema({
	owner: String,
	ownerName: String,
	description: String,
	attachedMedia: String,
	likes: Array,
	comments: Array
});

export default mongoose.model('Post', Post);
